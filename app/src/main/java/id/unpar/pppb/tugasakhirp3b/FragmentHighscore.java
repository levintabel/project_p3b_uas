package id.unpar.pppb.tugasakhirp3b;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import id.unpar.pppb.tugasakhirp3b.Engine.GameEngine;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHighscore extends Fragment implements View.OnClickListener {
    protected GameMenuListener gameMenuListener;
    protected Button button_mainmenu;
    protected Button button_restart;
    protected TextView textView_highscore;
    protected TextView textView_yourscore;

    protected GameEngine gameEngine;


    public FragmentHighscore() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View result = inflater.inflate(R.layout.fragment_fragment_highscore, container, false);
        this.button_mainmenu = result.findViewById(R.id.button_mainmenu);
        this.button_restart = result.findViewById(R.id.button_restart);
        button_restart.setOnClickListener(this);
        button_mainmenu.setOnClickListener(this);

        this.button_mainmenu.setOnClickListener(this);
        this.button_restart.setOnClickListener(this);

        this.textView_highscore = result.findViewById(R.id.text_highscore);
        this.textView_yourscore = result.findViewById(R.id.text_yourscore);

        textView_highscore.setText("High score: "+ gameEngine.getHighestScore());
        textView_yourscore.setText("Your score:" + gameEngine.getCurrentScore());

        return result;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==button_mainmenu.getId()){
            this.gameMenuListener.goToFragmentStart();
        }else if(v.getId()==button_restart.getId()){
            gameEngine.triggerStartGame();
        }
    }

    public static FragmentHighscore newInstance(GameMenuListener gameMenuListener, GameEngine gameEngine){
        FragmentHighscore result = new FragmentHighscore();
        result.gameMenuListener = gameMenuListener;
        result.gameEngine = gameEngine;
        return result;
    }
}
