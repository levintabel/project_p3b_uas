package id.unpar.pppb.tugasakhirp3b.ViewModel;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import id.unpar.pppb.tugasakhirp3b.Engine.Model.Ball;

/**
 * Created by Amabel on 11/28/2018.
 */

public class BallVModel {
    private Ball ball;
    private Paint paint;

    private Rect boundary;
    private RectF selfBoundary;

    public BallVModel(Ball ball) {
        this.ball = ball;
        this.paint = new Paint();
        this.paint.setColor(ball.getColor());
    }

    public void setBoundary(Rect drawableBoundary) {
        this.boundary = drawableBoundary;
        this.selfBoundary = new RectF(
                drawableBoundary.left + ball.getRadius(),
                drawableBoundary.top + ball.getRadius(),
                drawableBoundary.right - ball.getRadius(),
                drawableBoundary.bottom - ball.getRadius()
        );
    }



    public void draw(Canvas canvas){
        if(selfBoundary != null) {
            // just making sure that everything is ok.
            if(this.ball.getX() < selfBoundary.left) {
                this.ball.setX( selfBoundary.left);
            } else if(this.ball.getX() > selfBoundary.right){
                this.ball.setX(selfBoundary.right);
            }

            if(this.ball.getY() < selfBoundary.top) {
                this.ball.setY(selfBoundary.top);
            } else if(this.ball.getY() > selfBoundary.bottom){
                this.ball.setY(selfBoundary.bottom);
            }
        }
        canvas.drawCircle(this.ball.getX(),this.ball.getY(),this.ball.getRadius(),this.paint);
    }
}
