package id.unpar.pppb.tugasakhirp3b;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import id.unpar.pppb.tugasakhirp3b.Engine.GameEngine;
import id.unpar.pppb.tugasakhirp3b.Helper.Adapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHistoryscore extends Fragment implements View.OnClickListener{
    protected GameMenuListener gameMenuListener;
    protected Button button_historyBack;
    protected Adapter adapter;
    protected ListView listView;

    protected GameEngine gameEngine;

    public FragmentHistoryscore() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View result = inflater.inflate(R.layout.fragment_fragment_historyscore, container, false);
        this.button_historyBack = result.findViewById(R.id.button_hitoryScoreback);
        button_historyBack.setOnClickListener(this);

        adapter = new Adapter(inflater, gameEngine.getAllScores());
        listView = result.findViewById(R.id.list_soore);
        this.listView.setAdapter(this.adapter);

        this.button_historyBack.setOnClickListener(this);

        return  result;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==button_historyBack.getId()){
            this.gameMenuListener.goToFragmentStart();
        }
    }

    public static FragmentHistoryscore newInstance(GameMenuListener gameMenuListener, GameEngine gameEngine){
        FragmentHistoryscore result = new FragmentHistoryscore();
        result.gameMenuListener = gameMenuListener;
        result.gameEngine = gameEngine;
        return result;
    }
}
