package id.unpar.pppb.tugasakhirp3b.Engine.Model;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;

import id.unpar.pppb.tugasakhirp3b.Engine.Helper.FileConfig;

/**
 * Created by Amabel on 11/28/2018.
 */

public class HighScore {
    private ArrayList<Long> scores;
    private long currentScore;
    private long highestScore;
    private FileConfig fileConfig;

    public HighScore(Context appContext) {
        fileConfig = new FileConfig("score.json", appContext);
        this.scores = new ArrayList<>();
    }

    /**
     * Saves the score listings.
     */
    public void save(){
        JSONArray jsonScores = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        // Will try to sort that first before saving.
        Collections.sort(scores, Collections.<Long>reverseOrder());

        for(int i = 0 ; i < scores.size() ; i++){
            try {
                jsonScores.put(i,scores.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            jsonObject.put("scores",jsonScores);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        fileConfig.write(jsonObject.toString());
    }

    public void endScoreSession(){
        this.scores.add(currentScore);
        save();
    }

    /**
     * Will try to load it first.
     */
    public void load(){
        try {
            String input = fileConfig.read();
            JSONObject JSONresult = new JSONObject(input);
            JSONArray jsonArray = JSONresult.getJSONArray("scores");

            long temp = 0;

            for(int i = 0 ; i < jsonArray.length() ; i++){
                scores.add(jsonArray.getLong(i));
                temp = Math.max(scores.get(i), temp);
            }

            this.highestScore = temp;
        } catch (FileNotFoundException e) {
            // filenya gngak ada, jadi nggak usah pusing.
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * FOR DEBUGGING PURPOSES ONLY. This will generate a score set, and save it
     * directly as a file.
     */
    public void generateFakeScores() {
        scores.add((long) 100);
        scores.add((long) 300);
        scores.add((long) 340);
        scores.add((long) 390);
        highestScore = 390;
        save();
    }

    public ArrayList<Long> getScores() {
        return scores;
    }


    public long getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(long currentScore) {
        this.currentScore = currentScore;
    }

    public long getHighestScore() {
        return highestScore;
    }
}
