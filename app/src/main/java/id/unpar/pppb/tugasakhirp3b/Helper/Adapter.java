package id.unpar.pppb.tugasakhirp3b.Helper;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import id.unpar.pppb.tugasakhirp3b.R;

public class Adapter extends BaseAdapter {
    protected ArrayList<Long> listScore;
    protected LayoutInflater inflater;

    public Adapter(LayoutInflater inflater,ArrayList<Long> listScore) {
        this.inflater = inflater;
        this.listScore = listScore;
    }

    @Override
    public int getCount() {
        return this.listScore.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listScore.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = this.inflater.inflate(R.layout.string_list_item, null);
            TextView tvNomor = convertView.findViewById(R.id.text_angka);
            TextView tvScore = convertView.findViewById(R.id.text_score);

            ViewHolder viewHolder = new ViewHolder(tvNomor, tvScore, position);
            convertView.setTag(viewHolder);
        }
        ViewHolder vh =(ViewHolder)convertView.getTag();;
        vh.setIndex(position+1);
        vh.setScore(listScore.get(position));

        return convertView;
    }
}

