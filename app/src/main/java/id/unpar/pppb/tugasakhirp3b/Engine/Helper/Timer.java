package id.unpar.pppb.tugasakhirp3b.Engine.Helper;

import android.os.CountDownTimer;

/**
 * Created by Amabel on 11/29/2018.
 */

public class Timer extends CountDownTimer{

    public Timer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long l) {

    }

    @Override
    public void onFinish() {

    }
}
