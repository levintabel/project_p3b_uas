package id.unpar.pppb.tugasakhirp3b.Engine.Helper;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import id.unpar.pppb.tugasakhirp3b.Engine.GameCore;
import id.unpar.pppb.tugasakhirp3b.MainActivity;

/**
 * Created by Amabel on 12/7/2018.
 */

public class SensorReader implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private GameCore gameCore;

    public SensorReader(SensorManager sensorManager, GameCore gameCore) {
        this.gameCore = gameCore;
        this.sensorManager = sensorManager;
        this.accelerometer = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public void start() {
        this.sensorManager.registerListener(this, this.accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void stop() {
        this.sensorManager.unregisterListener(this, this.accelerometer);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float speedX = sensorEvent.values[0];
        float speedY = sensorEvent.values[1];
        this.gameCore.sensorUpdate(speedX,speedY);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
