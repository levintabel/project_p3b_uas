package id.unpar.pppb.tugasakhirp3b;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import id.unpar.pppb.tugasakhirp3b.Engine.GameEngine;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentStart extends Fragment implements View.OnClickListener {
    protected GameMenuListener gameMenuListener;
    protected Button btn_start, btn_exit,button_history;

    protected GameEngine gameEngine;

    public FragmentStart() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View result =  inflater.inflate(R.layout.fragment_fragment_start, container, false);

        this.btn_start = result.findViewById(R.id.button_start);
        this.btn_exit = result.findViewById(R.id.button_exit);
        this.button_history = result.findViewById(R.id.button_historyscore);
        btn_start.setOnClickListener(this);
        btn_exit.setOnClickListener(this);
        button_history.setOnClickListener(this);

        btn_start.setOnClickListener(this);
        btn_exit.setOnClickListener(this);
        button_history.setOnClickListener(this);

        return result;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==btn_start.getId()) {
            this.gameMenuListener.goToFragmentGame();

        } else if(v.getId()==button_history.getId()) {
            this.gameMenuListener.gotToFragmentHistoryscore();

        } else if(v.getId()==btn_exit.getId()) {
            // Keluar dari game.
            System.exit(1);

        }
    }

    public static FragmentStart newInstance(GameMenuListener gameMenuListener, GameEngine gameEngine){
        FragmentStart result = new FragmentStart();
        result.gameMenuListener = gameMenuListener;
        result.gameEngine = gameEngine;
        return result;
    }
}
