package id.unpar.pppb.tugasakhirp3b.Engine.Helper;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import id.unpar.pppb.tugasakhirp3b.Engine.Model.HighScore;

/**
 * Configuration file
 * Bakal buat nyimpen nyimpen konfiguarasi dan kawan kawan. Dipake buat nyimpen string biasa.
 */

public class FileConfig {
    Context context;
    String configName;

    public FileConfig(String configName, Context context) {
        this.context = context;
        this.configName = configName;
    }

    /**
     * Tulis ke dalem file
     * @param text isi dalem teksnya
     */
    public void write(String text){
        try {
            FileWriter fileWriter = new FileWriter(context.getFileStreamPath(this.configName));
            fileWriter.append(text);
            fileWriter.flush();

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Ambil filenya, baca dan return isinya sebagai string.
     * @return isi dalem filenya
     * @throws FileNotFoundException
     */
    public String read() throws FileNotFoundException {
        InputStream input = new FileInputStream(context.getFileStreamPath(this.configName));
        BufferedReader in = new BufferedReader(new InputStreamReader(input));

        StringBuilder sb = new StringBuilder();
        String temp = "";
        try {
            while ((temp = in.readLine()) != null) {
                sb.append(temp).append('\n');
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
