package id.unpar.pppb.tugasakhirp3b.Engine;

public enum GameStatus {
    PLAYING,
    OVER,
    PAUSED
}
