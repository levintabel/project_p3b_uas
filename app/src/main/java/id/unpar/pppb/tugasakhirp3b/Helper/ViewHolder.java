package id.unpar.pppb.tugasakhirp3b.Helper;

import android.widget.TextView;

public class ViewHolder {
    private TextView textView1;
    private TextView textView2;
    private int index;

    public ViewHolder(TextView textView1, TextView textView2, int index) {
        this.textView1 = textView1;
        this.textView2 = textView2;
        this.index = index;
    }

    public TextView getTextView1() {
        return textView1;
    }

    public void setTextView1(TextView textView1) {
        this.textView1 = textView1;
    }

    public TextView getTextView2() {
        return textView2;
    }

    public void setTextView2(TextView textView2) {
        this.textView2 = textView2;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        textView1.setText(""+index+". ");
    }

    public void setScore(long score){
        textView2.setText(""+score);
    }

}
