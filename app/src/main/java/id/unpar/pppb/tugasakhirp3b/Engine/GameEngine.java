package id.unpar.pppb.tugasakhirp3b.Engine;

import android.arch.lifecycle.ViewModel;
import android.graphics.Rect;
import android.hardware.SensorManager;

import java.util.ArrayList;

import id.unpar.pppb.tugasakhirp3b.ViewModel.BallVModel;

public interface GameEngine {
    void triggerStartGame();
    void triggerStopGame();
    void triggerPauseGame();
    void triggerResumeGame();

    long getCurrentScore();
    ArrayList<Long> getAllScores();
    long getHighestScore();

    BallVModel getTargetBall(); // will return ViewModel of target ball position.
    BallVModel[] getMovingBall(); // will return ViewModel of moving ball.

    GameStatus getGameStatus();

    void onGameBoardReady();
    void setDrawableBoundary(Rect drawableBoundary);
    long getDurationLeft();
}
