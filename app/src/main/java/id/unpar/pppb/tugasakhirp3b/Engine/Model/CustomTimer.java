package id.unpar.pppb.tugasakhirp3b.Engine.Model;

import android.os.AsyncTask;

import id.unpar.pppb.tugasakhirp3b.Engine.GameCore;

public class CustomTimer extends AsyncTask<GameCore, Object, Object> {
    private long durations;
    private int sleepRate = 100; //ms

    @Override
    protected Object doInBackground(GameCore... gameCores) {
        while(durations > 0) {

            //do something.
            this.publishProgress(gameCores[0], durations);

            durations -= sleepRate;
            try {
                Thread.sleep(sleepRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return gameCores[0];
    }

    @Override
    protected void onProgressUpdate(Object... values) {

        GameCore data = (GameCore) values[0];
        data.timerCallback((long) values[1]);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Object o) {
        GameCore data = (GameCore) o;
        data.timerFinishCallback();
        super.onPostExecute(o);
    }

    public void topUpDurations(long msTime) {
        this.durations += msTime;
    }
    public long getDurations() {
        return  durations;
    }
}
