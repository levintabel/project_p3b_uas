package id.unpar.pppb.tugasakhirp3b.Engine.Model;

/**
 * Created by Amabel on 11/22/2018.
 */

public class Ball {
    protected float x;
    protected float y;
    protected float radius;
    protected int color;
    protected float coeficient=1;

    public float getBaseSpeed() {
        return baseSpeed;
    }

    public void setBaseSpeed(float baseSpeed) {
        this.baseSpeed = baseSpeed;
    }

    protected float baseSpeed = 0.2f;

    public Ball(float x, float y, float radius, int color) {
        this.setX(x);
        this.setY(y);
        this.setRadius(radius);
        this.setColor(color);
    }

    public void setCoeficient(float coeficient){
        this.coeficient = coeficient;
    }

    public boolean isIntersect(Ball otherBall){
        double distance = Math.sqrt(Math.pow(this.getX() -otherBall.getX(),2)+Math.pow(this.getY() -otherBall.getY(),2));
        if (distance<this.getRadius()+otherBall.getRadius()){
            return true;
        } else {
            return false;
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getRadius() {
        return radius;
    }

    public void move(float x, float y){
        this.setX(this.getX() + x*(coeficient + baseSpeed));
        this.setY(this.getY() + y*(coeficient + baseSpeed));
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
