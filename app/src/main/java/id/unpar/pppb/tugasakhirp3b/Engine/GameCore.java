package id.unpar.pppb.tugasakhirp3b.Engine;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Random;

import id.unpar.pppb.tugasakhirp3b.Engine.Helper.FileConfig;
import id.unpar.pppb.tugasakhirp3b.Engine.Helper.SensorReader;
import id.unpar.pppb.tugasakhirp3b.Engine.Model.Ball;
import id.unpar.pppb.tugasakhirp3b.Engine.Model.CustomTimer;
import id.unpar.pppb.tugasakhirp3b.Engine.Model.HighScore;
import id.unpar.pppb.tugasakhirp3b.ViewModel.BallVModel;

public class GameCore implements GameEngine {

    protected Ball targetBall;
    protected ArrayList<Ball> movingBall;
    protected GameStatus currentGameStatus;
    protected GameBoard gameBoard;
    protected SensorReader sensorReader;
    protected CustomTimer gameTicker;
    protected int round=1;

    protected Context mainContext;

    protected HighScore scores;

    protected Rect drawableBoundary;

    private boolean isGameInitialized = false;

    protected long currentDurations = 0;

    public GameCore(GameBoard gameBoard, Context appContext, SensorManager sensorManager){
        currentGameStatus = GameStatus.OVER;
        this.gameBoard = gameBoard;
        this.mainContext = appContext;
        sensorReader = new SensorReader(sensorManager, this);

        scores = new HighScore(mainContext);
        scores.load();

        // ball initialization
        movingBall = new ArrayList<>();
    }

    @Override
    public GameStatus getGameStatus() {
        return currentGameStatus;
    }



    @Override
    public void setDrawableBoundary(Rect drawableBoundary) {
        this.drawableBoundary = drawableBoundary;
    }

    @Override
    public long getDurationLeft() {
        return currentDurations;
    }

    @Override
    public void triggerStartGame() {
        if(!isGameInitialized) {
            // TODO: Pisah ke fungsi khusus?
            gameBoard.onGameInit();
            isGameInitialized = true;
        }
        round = 1;
        scores.setCurrentScore(0);

        // this will trigger gameboard to initialize, and wait until it's ready.
        // then the gameboard will trigger onGameBoardReady.
        gameBoard.onGameStart();
    }

    @Override
    public void onGameBoardReady() {
        gameTicker = new CustomTimer();
        gameTicker.topUpDurations(10000);

        setUpNewRound();
        gameBoard.onGameTicks(GameEvent.OBJECT_SCORE); // update the score away!

        gameTicker.execute(this);
        sensorReader.start();
    }

    @Override
    public void triggerStopGame() {
        if(gameTicker != null && gameTicker.getStatus() != AsyncTask.Status.FINISHED) {
            gameTicker.cancel(true);
        }
        gameTicker = null;
        sensorReader.stop();
        scores.endScoreSession(); // will save the scores.
        gameBoard.onGameEnd();
    }

    @Override
    public void triggerPauseGame() {
        ///tbd
    }

    @Override
    public void triggerResumeGame() {
        //tbd
    }

    @Override
    public long getCurrentScore() {
        return scores.getCurrentScore();
    }

    @Override
    public ArrayList<Long> getAllScores() {
        return scores.getScores();
    }

    @Override
    public long getHighestScore() {
        return scores.getHighestScore();
    }


    @Override
    public BallVModel getTargetBall() {
        BallVModel temp = new BallVModel(targetBall);
        temp.setBoundary(drawableBoundary);
        return temp;
    }

    @Override
    public BallVModel[] getMovingBall() {
        BallVModel[] res = new BallVModel[movingBall.size()];
        for (int i = 0; i<movingBall.size(); i++) {
            res[i] = new BallVModel(movingBall.get(i));
            res[i].setBoundary(drawableBoundary);
        }
        return res;
    }


    public void setUpNewRound(){
        Random rn = new Random();
        movingBall.clear();
        for(int i=0; i<round; i++){
            Ball kambing = new Ball(
                    rn.nextFloat()*drawableBoundary.width(),
                    rn.nextFloat()*drawableBoundary.height(),
                    25,
                    Color.BLUE);
            kambing.setCoeficient(rn.nextFloat() * 2f);
            movingBall.add(kambing);
        }
        targetBall = new Ball(
                rn.nextFloat()*drawableBoundary.width(),
                rn.nextFloat()*drawableBoundary.width(),
                40,
                Color.DKGRAY);
        targetBall.setCoeficient(0.1f);
        targetBall.setBaseSpeed(0);
        round++;
    }

    /**
     * Provides an API for underlying models that can feed us information from sensors.
     * @param speedX
     * @param speedY
     */
    public void sensorUpdate(float speedX , float speedY){
        for(Ball b:movingBall)
            b.move(-speedX,speedY);

        if(round > 5){
            targetBall.move(-speedX, speedY);
        }
        gameBoard.onGameTicks(GameEvent.OBJECT_MOVE);

    }

    public void timerCallback(long durations) {
        this.currentDurations = durations;
        gameBoard.onGameTicks(GameEvent.TIMER_TICKS);

        //update the score or something.
        for(int i=0; i<movingBall.size(); i++){
            if(movingBall.get(i).isIntersect(targetBall)) {
                movingBall.remove(i);
            }
        }

        if(movingBall.isEmpty()){
            // plus point!
            long tambah = 20;
            tambah += gameTicker.getDurations()/1000;
            scores.setCurrentScore(scores.getCurrentScore() + tambah);
            gameBoard.onGameTicks(GameEvent.OBJECT_SCORE);
            gameTicker.topUpDurations(5000); //REWARDS!
            setUpNewRound();
        }
    }

    public void timerFinishCallback(){
        this.triggerStopGame();
    }
}
