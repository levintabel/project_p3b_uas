package id.unpar.pppb.tugasakhirp3b;

import android.hardware.SensorManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.unpar.pppb.tugasakhirp3b.Engine.GameBoard;
import id.unpar.pppb.tugasakhirp3b.Engine.GameCore;
import id.unpar.pppb.tugasakhirp3b.Engine.GameEngine;
import id.unpar.pppb.tugasakhirp3b.Engine.GameEvent;
import id.unpar.pppb.tugasakhirp3b.R;

public class MainActivity extends AppCompatActivity implements GameMenuListener, GameBoard{
    FragmentStart fragmentStart;
    FragmentGame fragmentGame;
    FragmentHighscore fragmentHighscore;
    FragmentHistoryscore fragmentHistoryscore;
    FragmentManager fragmentManager;
    GameEngine gameEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gameEngine = new GameCore(this, this.getApplicationContext(), (SensorManager)getSystemService(SENSOR_SERVICE));

        this.fragmentGame = FragmentGame.newInstance(this, gameEngine);
        this.fragmentStart = FragmentStart.newInstance(this, gameEngine);
        this.fragmentHighscore = FragmentHighscore.newInstance(this, gameEngine);
        this.fragmentHistoryscore = FragmentHistoryscore.newInstance(this, gameEngine);

        fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frag_container,this.fragmentStart);
        fragmentTransaction.commit();

    }

    @Override
    public void goToFragmentGame() {
        gameEngine.triggerStartGame();
    }

    @Override
    public void goToFragmentStart() {
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frag_container,this.fragmentStart);
        fragmentTransaction.commit();
    }

    @Override
    public void goToGameScoreFragment() {
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frag_container,this.fragmentHighscore);
        fragmentTransaction.commit();
    }

    @Override
    public void gotToFragmentHistoryscore() {
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frag_container,this.fragmentHistoryscore);
        fragmentTransaction.commit();
    }

    @Override
    public void onGameTicks(GameEvent event) {
        this.fragmentGame.onGameTicks(event);
    }

    @Override
    public void onGameEnd() {
        //display game score and etc
        goToGameScoreFragment();
    }

    @Override
    public void onGameStart() {
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frag_container,this.fragmentGame);
        fragmentTransaction.commit();
    }

    @Override
    public void onGameInit() {
        //isi kalo butuh inisialisasi di level aplikasi
    }
}
