package id.unpar.pppb.tugasakhirp3b.Engine;

public enum GameEvent {
    OBJECT_MOVE,
    OBJECT_SCORE,
    TIMER_TICKS
}
