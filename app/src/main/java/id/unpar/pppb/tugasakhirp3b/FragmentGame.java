package id.unpar.pppb.tugasakhirp3b;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.icu.text.Normalizer2;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import id.unpar.pppb.tugasakhirp3b.Engine.GameEngine;
import id.unpar.pppb.tugasakhirp3b.Engine.GameEvent;
import id.unpar.pppb.tugasakhirp3b.ViewModel.BallVModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGame extends Fragment implements View.OnClickListener {
    private static final String DEBUG_LOG = "FGAME";
    protected GameMenuListener gameMenuListener;
    protected Button button_endGame;
    protected TextView lblTimer, lblScore;

    protected GameEngine gameEngine;

    protected Bitmap bmpGame;
    protected Canvas cnvGame;
    protected ImageView imgGameViews;

    public FragmentGame() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View result = inflater.inflate(R.layout.fragment_fragment_game, container, false);
        this.button_endGame = result.findViewById(R.id.buttonEndGame);
        button_endGame.setOnClickListener(this);

        imgGameViews = result.findViewById(R.id.iv_canvas);

        button_endGame.setOnClickListener(this);
        lblTimer = result.findViewById(R.id.timerTextView);
        lblScore = result.findViewById(R.id.scoreTextView);

        // runs initialization once the view was mounted to the layout.
        result.post(new Runnable() {
            @Override
            public void run() {
                do_init();
            }
        });

        return  result;
    }

    public static FragmentGame newInstance(GameMenuListener gameMenuListener, GameEngine gameEngine){
        FragmentGame result = new FragmentGame();
        result.gameMenuListener = gameMenuListener;
        result.gameEngine = gameEngine;
        return result;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==button_endGame.getId()){
            gameEngine.triggerStopGame();
        }
    }



    public void do_init() {
        Log.i(DEBUG_LOG, "Play fragment initialized");
        // init image, dkk untuk level game di sini.
        bmpGame = Bitmap.createBitmap(imgGameViews.getMeasuredWidth(), imgGameViews.getMeasuredHeight(), Bitmap.Config.ARGB_4444);
        cnvGame = new Canvas(bmpGame);
        imgGameViews.setImageBitmap(bmpGame);

        gameEngine.setDrawableBoundary(new Rect(0,0, imgGameViews.getMeasuredWidth(), imgGameViews.getMeasuredHeight()));
        // don't forget to inform the engine if we're ready.
        gameEngine.onGameBoardReady();
    }

    public void onGameTicks(GameEvent event) {
        //update benda benda disini.
        if(event == GameEvent.OBJECT_MOVE){
            Log.i(DEBUG_LOG, "Gameticks event!");
            // clear benda!

            cnvGame.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

            gameEngine.getTargetBall().draw(cnvGame);
            for(BallVModel b: gameEngine.getMovingBall())
                    b.draw(cnvGame);

                imgGameViews.invalidate();
        } else if(event == GameEvent.TIMER_TICKS) {
            lblTimer.setText(String.format("%.02f", gameEngine.getDurationLeft()/1000f));
        } else if(event == GameEvent.OBJECT_SCORE) {
            lblScore.setText("Score: " + gameEngine.getCurrentScore());
        }

    }



}
