package id.unpar.pppb.tugasakhirp3b.Engine;

public interface GameBoard {
    void onGameTicks(GameEvent event);

    void onGameEnd();
    void onGameStart();

    void onGameInit();
}
