package id.unpar.pppb.tugasakhirp3b;

public interface GameMenuListener {
    void goToFragmentGame();
    void goToFragmentStart();
    void goToGameScoreFragment();
    void gotToFragmentHistoryscore();
}
